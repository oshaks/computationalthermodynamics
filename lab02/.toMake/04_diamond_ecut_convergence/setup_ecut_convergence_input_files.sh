#!/bin/bash

for testCut in {15..50..5}; do
    sed -e 's/xxCUTxx/'$testCut'/g' C_diamond.in > "C_diamond_"$testCut".in"
done
