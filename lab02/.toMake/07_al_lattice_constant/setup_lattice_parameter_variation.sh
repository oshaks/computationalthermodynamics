#!/bin/bash

#Lattice constants to test
latticeParameterList="3.7 3.8 3.9 4.0 4.1 4.2 4.3 4.4 4.5 4.6 4.7"

#For loop to setup qe input file, and run each qe calculation
for testValue in $latticeParameterList ; do
    sed -e 's/xxALATxx/'$testValue'/g' 1_al_template.in > "1_al_"$testValue".in"
    pw.x < "1_al_"$testValue".in" > "1_al_"$testValue".out" &
    wait
done
wait

#Collect results
echo "a(Ang) energy(Ry)" > aLatt_energy.dat
for testValue in $latticeParameterList ; do
	e=$(grep ! "1_al_"$testValue".out" | awk '{print $5}')
	echo $testValue $e  >> aLatt_energy.dat
done
