f(x)=a+b*x+c*x**2+d*x**3+e*x**4
a=1; b=1; c=1; d=1
fit f(x) "aLatt_energy.dat" via a, b, c, d, e
set title "Energy vs lattice constant"
set xlabel "Lattice constant (Ang)"
set ylabel "Energy (Ry)"
set term png
set output "approx.png"
p "< tail -n +2 aLatt_energy.dat" u 1:2 t "data points", f(x) t "4th order approximation"

