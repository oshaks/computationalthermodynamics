f(x)=a+b*x+c*x**2+d*x**3+e*x**4
a=1; b=1; c=1; d=1
fit f(x) "phonon_dos_perfect_supercell.dat" via a, b, c, d, e
set title "Phonon DOS and polynomial approximation"
set xlabel "DOS (1/THz)"
set ylabel "Frequency (THz)"
set yrange [0:]
set xrange [0:22]
set term png
set output "approx.png"
p "phonon_dos_perfect_supercell.dat" u 1:2 t "Phonon DOS data points", f(x) t "A (bad) polynomial approx" 
