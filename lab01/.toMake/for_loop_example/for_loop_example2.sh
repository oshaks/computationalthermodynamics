#!/bin/bash

list_of_files=$(echo *.dat)
N="25"

counter=0
for file in $list_of_files
do
	let counter=counter+1
	echo "File" $counter", first N="$N" characters of first line:"
	head -n 1 $file | cut -c -$N
	echo ""
done
