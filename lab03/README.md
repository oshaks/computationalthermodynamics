Lab 3: Points defects
===============================

[Back to Course Overview](..)

Objectives
----------------------
1. Create a ZrC conventional unit cell and relax the lattice parameter.
2. Create a ZrC supercell manually, and using software. 
3. Use DFT calculations to determine point defect concentrations for vacancies, antisite and Frenkel defects in ZrC.
4. Calculate vacancy formation energy for Al including temperature-dependent phonon and electron free energy terrms.

![zrc_unitcell](.images/zrc.png)

### 1. ZrC conventional unit cell relaxation. 

ZrC is rocksalt structured so the conventional unit cell is well known experimentally and computationally. Download the conventional unit cell `.cif` file from [Materials Project database](https://materialsproject.org/materials/mp-2795/#snl). Open the `.cif` file with VESTA and examine the structure. Export it to a format, such as POSCAR, from which we can build a quantum-espresso input file. 

Create a directory `01_ZrC_relax_conventional`, and make the quantum-espresso input file, `zrc_relax_conventional.in`, for the ZrC conventional unit cell. This should look like: 
```
&CONTROL
   calculation = 'vc-relax' ! Variable cell relaxati
   nstep  = 50              ! Default number of optimisation steps
   forc_conv_thr = 1.0D-3   ! Ionic force convergence threshold (a.u.)
   etot_conv_thr = 1.0d-5   ! Ionic energy convergence threshold (a.u.)
   pseudo_dir = '.'
/

&SYSTEM
   ibrav = 0             ! Bravais Lattice index -- 2 for fcc
   nat = 8               ! Num of atoms in cell
   ntyp = 2              ! Num of atom types
   ecutwfc = 46.0        ! Cutoff energy in Rydberg, 46 suggested minimum
   occupations='smearing'! Smearing occupation for metals
   degauss = 1.0d-01     ! Smearing width in Ry
   smearing='mp'         ! Methfessel-Paxton smearing
/

&ELECTRONS
   conv_thr = 1.0d-8     ! SCF convergence in Ry
   mixing_beta = 0.7     ! SCF mixing factor
/

&IONS
  ion_dynamics = 'bfgs'  ! Broyden-Fletcher-Goldfarb-Shanno quasi-Newton method
/

&CELL
  cell_dynamics = 'bfgs' ! Broyden-Fletcher-Goldfarb-Shanno quasi-Newton method
  press = 0              ! Pressure default value in KBar
  press_conv_thr = 0.5   ! Default value in KBar
  cell_dofree = 'all'    ! Relax cell volume, angles, x/y/z, etc..
/

ATOMIC_SPECIES
  Zr 91.224 Zr.pbe-spn-kjpaw_psl.1.0.0.UPF ! Species, mass, pseudopotential
  C  12.011 C.pbe-n-kjpaw_psl.1.0.0.UPF

CELL_PARAMETERS (ANGSTROM)
  4.724340 0.000000 0.000000     ! A lattice vector
  0.000000 4.724340 0.000000     ! B lattice vector
  0.000000 0.000000 4.724340     ! C lattice vector

ATOMIC_POSITIONS crystal
  Zr 0.000000 0.000000 0.000000
  Zr 0.500000 0.500000 0.000000
  Zr 0.500000 0.000000 0.500000
  Zr 0.000000 0.500000 0.500000
  C  0.000000 0.000000 0.500000
  C  0.500000 0.000000 0.000000
  C  0.000000 0.500000 0.000000
  C  0.500000 0.500000 0.500000

K_POINTS automatic
  4 4 4 1 1 1             ! 2x2x2 k-point grid, offset by 1/2 in xyz
```
Some notes about this input file:
- We already have a carbon PBE PAW potential from previous calculations. For the Zr potential, download a PBE PAW pseudopotential from the quantum-espresso [repository](http://quantumespresso.org/pseudopotentials/ps-library/zr). 
You can do this using the command `wget http://quantumespresso.org/upf_files/Zr.pbe-spn-kjpaw_psl.1.0.0.UPF`. 
- In the input file, the cutoff energy is set to the minimum suggested for the Zr pseudopotential. You can see this by typing `head Zr.pbe-spn-kjpaw_psl.1.0.0.UPF`. As minimum cutoff for the calculation, use the largest of the two suggested minimum cutoffs (46 Ry for Zr, and 40 for C). Outside of initial quick tests, and illustrative calculations like in this session, the cutoff should be converged with respect to whatever the property of interest being calculated is.
- `ibrav` is set to zero, as we using a conventional unit cell rather than the fcc Bravais one (`ibrav=2`). 
- In this instance, we will allow quantum-espresso to determine the optimum lattice parameter using the variable-cell `vc-relax` setting, as an alternative to doing the optimisation by hand as previously, where we computed the total energy at different expansions and fitted a polynomial to minimise. Using `vc-relax` is convenient but may be less accurate. To enable the crystal relaxation, `&IONS` and `&CELL` cards are introduced. 
- Note smearing is set to quite a high value `degauss = 1.0d-01`. This is an attempt to aid convergence, as ZrC is a metallic system (despite being also being classified as a ceramic), so requires quite high k-point sampling. However high k-point sampling is expensive for large cells or low symmetry systems, which is geneally what we have in defect systems, so for the purposes of this short course we are being pragmatic and using quite low k-point sampling. For production calculations, kpoints and smearing width should be converged like cutoff energy.

Run the conventional unit cell DFT calculation with usual command, `pw.x < zrc_relax_conventional.in > zrc_relax_conventional.out &`. This should take about 2-3 minutes. Examine the output file zrc_relax_conventional.out. 

Inspect the change in energy and crystal structure as the cell relaxes by grepping the total energy with `!` and lattice parameter with `CELL_PARAMETERS` matches: 
```
tmellan$ grep ! *out
!    total energy              =   -1303.09457485 Ry
!    total energy              =   -1303.09522891 Ry
!    total energy              =   -1303.09524137 Ry
!    total energy              =   -1303.09524137 Ry
!    total energy              =   -1303.09504229 Ry
tmellan$ grep -A 2 CELL_PARAMETERS *in *out
"zrc_relax_conventional.in:CELL_PARAMETERS (ANGSTROM)
zrc_relax_conventional.in-  4.724340 0.000000 0.000000     ! A lattice vector
zrc_relax_conventional.in-  0.000000 4.724340 0.000000     ! B lattice vector
--
zrc_relax_conventional.out:CELL_PARAMETERS (angstrom)
zrc_relax_conventional.out-   4.704503538   0.000000000   0.000000000
zrc_relax_conventional.out-   0.000000000   4.704503538   0.000000000
--
zrc_relax_conventional.out:CELL_PARAMETERS (angstrom)
zrc_relax_conventional.out-   4.706911018   0.000000000   0.000000000
zrc_relax_conventional.out-   0.000000000   4.706911018   0.000000000
--
zrc_relax_conventional.out:CELL_PARAMETERS (angstrom)
zrc_relax_conventional.out-   4.706875154   0.000000000   0.000000000
zrc_relax_conventional.out-   0.000000000   4.706875154   0.000000000
--
zrc_relax_conventional.out:CELL_PARAMETERS (angstrom)
zrc_relax_conventional.out-   4.706875154   0.000000000   0.000000000
zrc_relax_conventional.out-   0.000000000   4.706875154   0.000000000
(base) tmellan@ImperialCollege:~/.../new/01_ZrC_relax_conventional$
```
The experimentally reported lattice parameter for ZrC is about 4.699 Angstrom according to [Savvatimskiy et al](https://www.cambridge.org/core/journals/journal-of-materials-research/article/measurement-of-zrc-properties-up-to-5000-k-by-fast-electrical-pulse-heating-method/767F7AE0CD4D9508F0CCD611F5A8BFDE). 

#### _Task_ (optional)
1. Setup a conventional unit cell vc-relaxation calculation for aluminium. 
2. How does the lattice parameter compare to the previous we found doing the optimisation by fitting polynomials?

### 2. ZrC supercell SCF calculations

#### _Task_ - manually make a 2x1x1 supercell of  ZrC
1. From the previous section we have a relaxed ZrC unit cell. Make a directory `02_ZrC_211_scf` and create a 2x1x1 supercell of the relaxed conventional ZrC unit cell. 
2. Modify the previous ZrC calculation input file to do an SCF calculation for the 2x1x1 cell. (Make sure kpoint sampling remains commensurate along the expanded direction.)
3. Check your 2x1x1 cell total energy is sensible by comparing with the 1x1x1 cell total energy.

#### 2x2x2 supercell

For larger supercells it is inconvenient to build the supercell by hand, instead we will use the [phonopy](https://atztogo.github.io/phonopy/). Other options exist, like using the python Atomic Simulation Environment[(ASE)](https://wiki.fysik.dtu.dk/ase/) or VESTA.

Start with the relaxed unit cell in the form of a quantum-espresso input file called `zrc_relaxed_1x1x1.in`:
```
&CONTROL
   calculation = 'scf' ! Variable cell relaxation
   nstep  = 50              ! Default number of optimisation steps
   forc_conv_thr = 1.0D-3   ! Ionic force convergence threshold (a.u.)
   etot_conv_thr = 1.0d-5   ! Ionic energy convergence threshold (a.u.)
   pseudo_dir = '.'
/

&SYSTEM
   ibrav = 0             ! Bravais Lattice index -- 2 for fcc
   nat = 8               ! Num of atoms in cell
   ntyp = 2              ! Num of atom types
   ecutwfc = 46.0        ! Cutoff energy in Rydberg, 46 suggested minimum
   occupations='smearing'! Smearing occupation for metals
   degauss = 1.0d-01     ! Smearing width in Ry
   smearing='mp'         ! Methfessel-Paxton smearing
/

&ELECTRONS
   conv_thr = 1.0d-8     ! SCF convergence in Ry
   mixing_beta = 0.7     ! SCF mixing factor
/

ATOMIC_SPECIES
  Zr 91.224 Zr.pbe-spn-kjpaw_psl.1.0.0.UPF ! Species, mass, pseudopotential
  C  12.011 C.pbe-n-kjpaw_psl.1.0.0.UPF

CELL_PARAMETERS (ANGSTROM)
  4.706875154 0.000000 0.000000     ! A lattice vector
  0.000000 4.706875154 0.000000     ! B lattice vector
  0.000000 0.000000 4.706875154     ! C lattice vector

ATOMIC_POSITIONS crystal
  Zr 0.000000 0.000000 0.000000
  Zr 0.500000 0.500000 0.000000
  Zr 0.500000 0.000000 0.500000
  Zr 0.000000 0.500000 0.500000
  C  0.000000 0.000000 0.500000
  C  0.500000 0.000000 0.000000
  C  0.000000 0.500000 0.000000
  C  0.500000 0.500000 0.500000

K_POINTS automatic
  4 4 4 1 1 1             ! 2x2x2 k-point grid, offset by 1/2 in xyz
```
Build a 2x2x2 supercell with the phonopy command `phonopy --qe -d --dim="2 2 2" -c zrc_relaxed_1x1x1.in`. This will produce a file `supercell.in`, that you can use to make the 2x2x2 calculation input file, `zrc_222_scf.in`.

Check the cell parameters, number of atoms and k-point sampling are correct. Check the units for the cell parameters are correct (phonopy may tell you the supercell parameters are in Bohr, incorrectly, if the initial file parameters were in Angstrom).

The 2x2x2 input file `zrc_222_scf.in` should look like:
```
&CONTROL
   calculation = 'scf' ! Variable cell relaxation
   nstep  = 50              ! Default number of optimisation steps
   forc_conv_thr = 1.0D-3   ! Ionic force convergence threshold (a.u.)
   etot_conv_thr = 1.0d-4   ! Ionic energy convergence threshold (a.u.)
   pseudo_dir = '.'
/

&SYSTEM
   ibrav = 0             ! Bravais Lattice index -- 2 for fcc
   nat = 64               ! Num of atoms in cell
   ntyp = 2              ! Num of atom types
   ecutwfc = 46.0        ! Cutoff energy in Rydberg, 46 suggested minimum
   occupations='smearing'! Smearing occupation for metals
   degauss = 1.0d-01     ! Smearing width in Ry
   smearing='mp'         ! Methfessel-Paxton smearing
/

&ELECTRONS
   conv_thr = 1.0d-8     ! SCF convergence in Ry
   mixing_beta = 0.7     ! SCF mixing factor
/

K_POINTS automatic
  2 2 2 1 1 1             ! 2x2x2 k-point grid, offset by 1/2 in xyz

CELL_PARAMETERS (Angstrom)
    9.4137503079999991    0.0000000000000000    0.0000000000000000
    0.0000000000000000    9.4137503079999991    0.0000000000000000
    0.0000000000000000    0.0000000000000000    9.4137503079999991
ATOMIC_SPECIES
 Zr   91.22400   Zr.pbe-spn-kjpaw_psl.1.0.0.UPF
  C   12.01070   C.pbe-n-kjpaw_psl.1.0.0.UPF
ATOMIC_POSITIONS crystal
 Zr   0.0000000000000000  0.0000000000000000  0.0000000000000000
 Zr   0.5000000000000000  0.0000000000000000  0.0000000000000000
 Zr   0.0000000000000000  0.5000000000000000  0.0000000000000000
 Zr   0.5000000000000000  0.5000000000000000  0.0000000000000000
 Zr   0.0000000000000000  0.0000000000000000  0.5000000000000000
 Zr   0.5000000000000000  0.0000000000000000  0.5000000000000000
 Zr   0.0000000000000000  0.5000000000000000  0.5000000000000000
 Zr   0.5000000000000000  0.5000000000000000  0.5000000000000000
 Zr   0.2500000000000000  0.2500000000000000  0.0000000000000000
 Zr   0.7500000000000000  0.2500000000000000  0.0000000000000000
 Zr   0.2500000000000000  0.7500000000000000  0.0000000000000000
 Zr   0.7500000000000000  0.7500000000000000  0.0000000000000000
 Zr   0.2500000000000000  0.2500000000000000  0.5000000000000000
 Zr   0.7500000000000000  0.2500000000000000  0.5000000000000000
 Zr   0.2500000000000000  0.7500000000000000  0.5000000000000000
 Zr   0.7500000000000000  0.7500000000000000  0.5000000000000000
 Zr   0.2500000000000000  0.0000000000000000  0.2500000000000000
 Zr   0.7500000000000000  0.0000000000000000  0.2500000000000000
 Zr   0.2500000000000000  0.5000000000000000  0.2500000000000000
 Zr   0.7500000000000000  0.5000000000000000  0.2500000000000000
 Zr   0.2500000000000000  0.0000000000000000  0.7500000000000000
 Zr   0.7500000000000000  0.0000000000000000  0.7500000000000000
 Zr   0.2500000000000000  0.5000000000000000  0.7500000000000000
 Zr   0.7500000000000000  0.5000000000000000  0.7500000000000000
 Zr   0.0000000000000000  0.2500000000000000  0.2500000000000000
 Zr   0.5000000000000000  0.2500000000000000  0.2500000000000000
 Zr   0.0000000000000000  0.7500000000000000  0.2500000000000000
 Zr   0.5000000000000000  0.7500000000000000  0.2500000000000000
 Zr   0.0000000000000000  0.2500000000000000  0.7500000000000000
 Zr   0.5000000000000000  0.2500000000000000  0.7500000000000000
 Zr   0.0000000000000000  0.7500000000000000  0.7500000000000000
 Zr   0.5000000000000000  0.7500000000000000  0.7500000000000000
  C   0.0000000000000000  0.0000000000000000  0.2500000000000000
  C   0.5000000000000000  0.0000000000000000  0.2500000000000000
  C   0.0000000000000000  0.5000000000000000  0.2500000000000000
  C   0.5000000000000000  0.5000000000000000  0.2500000000000000
  C   0.0000000000000000  0.0000000000000000  0.7500000000000000
  C   0.5000000000000000  0.0000000000000000  0.7500000000000000
  C   0.0000000000000000  0.5000000000000000  0.7500000000000000
  C   0.5000000000000000  0.5000000000000000  0.7500000000000000
  C   0.2500000000000000  0.0000000000000000  0.0000000000000000
  C   0.7500000000000000  0.0000000000000000  0.0000000000000000
  C   0.2500000000000000  0.5000000000000000  0.0000000000000000
  C   0.7500000000000000  0.5000000000000000  0.0000000000000000
  C   0.2500000000000000  0.0000000000000000  0.5000000000000000
  C   0.7500000000000000  0.0000000000000000  0.5000000000000000
  C   0.2500000000000000  0.5000000000000000  0.5000000000000000
  C   0.7500000000000000  0.5000000000000000  0.5000000000000000
  C   0.0000000000000000  0.2500000000000000  0.0000000000000000
  C   0.5000000000000000  0.2500000000000000  0.0000000000000000
  C   0.0000000000000000  0.7500000000000000  0.0000000000000000
  C   0.5000000000000000  0.7500000000000000  0.0000000000000000
  C   0.0000000000000000  0.2500000000000000  0.5000000000000000
  C   0.5000000000000000  0.2500000000000000  0.5000000000000000
  C   0.0000000000000000  0.7500000000000000  0.5000000000000000
  C   0.5000000000000000  0.7500000000000000  0.5000000000000000
  C   0.2500000000000000  0.2500000000000000  0.2500000000000000
  C   0.7500000000000000  0.2500000000000000  0.2500000000000000
  C   0.2500000000000000  0.7500000000000000  0.2500000000000000
  C   0.7500000000000000  0.7500000000000000  0.2500000000000000
  C   0.2500000000000000  0.2500000000000000  0.7500000000000000
  C   0.7500000000000000  0.2500000000000000  0.7500000000000000
  C   0.2500000000000000  0.7500000000000000  0.7500000000000000
  C   0.7500000000000000  0.7500000000000000  0.7500000000000000
```

#### _Task_ (optional)
1. Inspect the structure using VESTA.
2. Start the DFT calculation, examining the output, allowing it to run until the first step of the SCF cycle has completed (2-3 mins).

---

The 2x2x2 ZrC DFT calculation with 64 atoms and 512 electrons is quite a serious job. Even with our low-value parameters, it will take around 20 mins to complete. If the perfect fcc symmetry is broken by introducing defects, the SCF convergence time will increase substantially. 


## 3. Point defect DFT calculations for ZrC

To do supercell dilute limit defect calculations, we must make the supercell large enough so that defects are screened from their periodic images. If the supercell is too small there will for example be a spurious interaction between a vacancy and it's own elastic deformation field. In general fairly large cells are needed, like 2x2x2, 3x3x3 or 4x4x4 supercells, where the size must be determined by convergence tests, e.g. with respect to defect formation energy. Roughly speaking, in metals around 10-20 Angstrom is often a reasonable cell dimension. 

As we are limited in time, we will be pragmatic in order to illustrate the practice of how to setup a defect DFT calculation. For ZrC we will put the defect in a 1x1x1 conventional unit cell, in order to make the calculation run fast enough. This violates the dilute limit approximation quite severely, but will nonethelews produce insightful results on the relatively stability of different types of defects in ZrC.  

*Using a unit cell for these calculations is definitely not a good idea for production calculations, and will result in errors of the order ~1 eV.* For the simpler Al system, we can afford to use a 2x2x2 for our vacancy calculations, which we will do later. In general using a 2x2x2 supercell may not be sufficient either -- the supercell expansion size must be converged with respect to the quantity of interest!

#### ZrC vacancy DFT calculation

Perform a SCF total energy calculation for ZrC with a vacancy on a Zr site. 

Make a directory `04_vacancy/zr_vac_scf` and create an input file for the relaxed conventional unit cell, with a vacancy at site `Zr 0.000000 0.000000 0.000000`. This should look like:

```
&CONTROL
   calculation = 'scf' ! Variable cell relaxation
   nstep  = 50              ! Default number of optimisation steps
!   forc_conv_thr = 1.0D-3   ! Ionic force convergence threshold (a.u.)
!   etot_conv_thr = 1.0d-4   ! Ionic energy convergence threshold (a.u.)
   pseudo_dir = '.'
   tprnfor = .true.    ! calculate interatomoic forces
   tstress = .true.    ! calculate stresses
/

&SYSTEM
   ibrav = 0             ! Bravais Lattice index -- 2 for fcc
   nat = 7               ! Num of atoms in cell
   ntyp = 2              ! Num of atom types
   ecutwfc = 46.0        ! Cutoff energy in Rydberg, 46 suggested minimum
   occupations='smearing'! Smearing occupation for metals
   degauss = 1.0d-01     ! Smearing width in Ry
   smearing='mp'         ! Methfessel-Paxton smearing
/

&ELECTRONS
   conv_thr = 1.0d-8     ! SCF convergence in Ry
   mixing_beta = 0.7     ! SCF mixing factor
/

ATOMIC_SPECIES
  Zr 91.224 Zr.pbe-spn-kjpaw_psl.1.0.0.UPF ! Species, mass, pseudopotential
  C  12.011 C.pbe-n-kjpaw_psl.1.0.0.UPF

CELL_PARAMETERS (ANGSTROM)
  4.706875154 0.000000 0.000000     ! A lattice vector
  0.000000 4.706875154 0.000000     ! B lattice vector
  0.000000 0.000000 4.706875154     ! C lattice vector

ATOMIC_POSITIONS crystal
  Zr 0.500000 0.500000 0.000000
  Zr 0.500000 0.000000 0.500000
  Zr 0.000000 0.500000 0.500000
  C  0.000000 0.000000 0.500000
  C  0.500000 0.000000 0.000000
  C  0.000000 0.500000 0.000000
  C  0.500000 0.500000 0.500000

K_POINTS automatic
  4 4 4 1 1 1             ! 2x2x2 k-point grid, offset by 1/2 in xyz
```
Before running the calculation, check the `ATOMIC_POSITIONS` and `nat` values.

#### _Task_
- Perform the following DFT calculations: 
	1.  a `calculation = 'SCF'` run for perfect ZrC on the relaxed 1x1x1 cell.
	2. `calculation = 'SCF'` run for the same cell with a Zr vacancy.
	3. `calculation = 'SCF'` run for a C vacancy.
- While the jobs are running, consider whether we should allow atom sites to relax, for example, perhaps those nearest-neighbour to the vacancy should relax to a lower energy configuration? Consider the constraints introduced due to the extreme approximation of using the 1x1x1 conventional cell rather a larger one, and the effect this will have on our defect cell energy.
 
### 4. Defect formation energy calculations for ZrC

#### Vacancy formation energy calculation
From the previous jobs, we should have obtained energies per atom that look like:
```
tmellan$ grep ! perfect_scf/*out | awk '{print $5/8}'
-162.887
tmellan$ grep ! zr_vac_scf/*out | awk '{print $5/7}'
-142.18
tmellan$ grep ! c_vac_scf/zrc.out | awk '{print $5/7}'
-183.502
```
These total energy values provide little insight so far. To compare the relative stability of different stoichiometries of ZrC, we need to use appropriate chemical potentials. For instance, consider placing the removed atoms of each species in reservoirs of diamond, for the carbon vacancy, and hexagonal Zr metal, for the Zr vacancy.

To compute the energy of Zr metal, start by finding a structure from the [Materials Project database](https://materialsproject.org/materials/mp-131/#). Make a `vc-relax` calculation, by analogy to the ZrC unit cell relaxation calculation, to find the minimum energy configuration. Note that all DFT calculations within a set used to compute energy differences should be calculated with the same number of planewaves, i.e., a cutoff energy of 46 Ry for our case. 

The hexagonal Zr metal input file should look like:
```
&CONTROL
   calculation = 'vc-relax' ! Variable cell relaxation
   nstep  = 50              ! Default number of optimisation steps
   forc_conv_thr = 1.0D-3   ! Ionic force convergence threshold (a.u.)
   etot_conv_thr = 1.0d-5   ! Ionic energy convergence threshold (a.u.)
   pseudo_dir = '.'
/

&SYSTEM
   ibrav = 0             ! Bravais Lattice index -- 2 for fcc
   nat = 2               ! Num of atoms in cell
   ntyp = 1              ! Num of atom types
   ecutwfc = 46.0        ! Cutoff energy in Rydberg, 46 suggested minimum
   occupations='smearing'! Smearing occupation for metals
   degauss = 1.0d-01     ! Smearing width in Ry
   smearing='mp'         ! Methfessel-Paxton smearing
/

&ELECTRONS
   conv_thr = 1.0d-8     ! SCF convergence in Ry
   mixing_beta = 0.7     ! SCF mixing factor
/

&IONS
  ion_dynamics = 'bfgs'  ! Broyden-Fletcher-Goldfarb-Shanno quasi-Newton method
/

&CELL
  cell_dynamics = 'bfgs' ! Broyden-Fletcher-Goldfarb-Shanno quasi-Newton method
  press = 0              ! Pressure default value in KBar
  press_conv_thr = 0.5   ! Default value in KBar
  cell_dofree = 'all'    ! Relax cell volume, angles, x/y/z, etc..
/

ATOMIC_SPECIES
 Zr  91.224   Zr.pbe-spn-kjpaw_psl.1.0.0.UPF

CELL_PARAMETERS (ANGSTROM)
3.239231 0.000000 0.000000
-1.619616 2.805257 0.000000
0.000000 0.000000 5.172220

ATOMIC_POSITIONS crystal
Zr 0.333333 0.666667 0.250000
Zr 0.666667 0.333333 0.750000

K_POINTS automatic
  4 4 4 1 1 1
```

For diamond, get a conventional unit cell from Materials Project database entry [mp-66](https://materialsproject.org/materials/mp-66/#). Make an input file for a `calculation = 'vc-relax'` relax job. This should look something like:

```
&CONTROL
   calculation = 'vc-relax' ! Variable cell relaxation
   nstep  = 50              ! Default number of optimisation steps
   forc_conv_thr = 1.0D-3   ! Ionic force convergence threshold (a.u.)
   etot_conv_thr = 1.0d-5   ! Ionic energy convergence threshold (a.u.)
   pseudo_dir = '.'
/

&SYSTEM
   ibrav = 0             ! Bravais Lattice index -- 2 for fcc
   nat = 8               ! Num of atoms in cell
   ntyp = 1              ! Num of atom types
   ecutwfc = 46.0        ! Cutoff energy in Rydberg, 46 suggested minimum
/

&ELECTRONS
   conv_thr = 1.0d-8     ! SCF convergence in Ry
   mixing_beta = 0.7     ! SCF mixing factor
/

&IONS
  ion_dynamics = 'bfgs'  ! Broyden-Fletcher-Goldfarb-Shanno quasi-Newton method
/

&CELL
  cell_dynamics = 'bfgs' ! Broyden-Fletcher-Goldfarb-Shanno quasi-Newton method
  press = 0              ! Pressure default value in KBar
  press_conv_thr = 0.5   ! Default value in KBar
  cell_dofree = 'all'    ! Relax cell volume, angles, x/y/z, etc..
/

ATOMIC_SPECIES
  C  12.011 C.pbe-n-kjpaw_psl.1.0.0.UPF

CELL_PARAMETERS (ANGSTROM)
3.573709 0.000000 0.000000
0.000000 3.573709 0.000000
0.000000 0.000000 3.573709

ATOMIC_POSITIONS crystal
C 0.000000 0.000000 0.500000
C 0.250000 0.250000 0.750000
C 0.500000 0.000000 0.000000
C 0.000000 0.500000 0.000000
C 0.500000 0.500000 0.500000
C 0.750000 0.250000 0.250000
C 0.750000 0.750000 0.750000
C 0.250000 0.750000 0.250000

K_POINTS automatic
  4 4 4 1 1 1
```

After relaxing the diamond and Zr structures, run SCF jobs to get accuracte total energies. During a relaxation the number of planewaves can change is the volume changes, so it can be important to rerun the final structures as single point SCF jobs for accurate results. 

Check the energies produced are close to the following:
```
tmellan$ grep ! *_scf/*out | column -t
C_diamond_scf/C_diamond.out:!    total energy              =    -147.46906910 Ry
c_vac_scf/zrc.out:!              total energy              =   -1284.51650624 Ry
perfect_scf/zrc.out:!            total energy              =   -1303.09504231 Ry
zr_metal_scf/zr.out:!            total energy              =    -614.41841918 Ry
zr_vac_scf/zrc.out:!             total energy              =    -995.25925222 Ry
```

Use these total energy values to find the Zr and C vacancy formation energies and to predict the vacancy concentration by following jupyter notebook [ZrC-point-defect-thermodynamics](.toMake/ZrC-point-defect-thermodynamics.pdf).

#### Anti-site defect calculation
Work out the anti-site defect formation energy for ZrC. Due to time limitations, use the Zr4C4 cell, swapping the positions of nearest-neighbour Zr and C sites. For example, use an input structure like:
```
! Switch Zr 0.000000 0.000000 0.000000 and C  0.000000 0.000000 0.500000, to
! Zr 0.000000 0.000000 0.500000 and C 0.000000 0.000000 0.000000
ATOMIC_POSITIONS crystal
  Zr 0.000000 0.000000 0.500000
  Zr 0.500000 0.500000 0.000000
  Zr 0.500000 0.000000 0.500000
  Zr 0.000000 0.500000 0.500000
  C  0.000000 0.000000 0.000000
  C  0.500000 0.000000 0.000000
  C  0.000000 0.500000 0.000000
  C  0.500000 0.500000 0.500000
```
Follow the jupyter notebook [ZrC-point-defect-thermodynamics](.toMake/ZrC-point-defect-thermodynamics.pdf) to determine whether anti-site defects are more or less favourable than Zr or C vacancies.

#### Frenkel defect calculations

Calculate the total energy of a bound-pair Frenkel defect in ZrC. Remove a C atom from the perfect site and place it in an interstitial configuration, then allow the configuration of the intersitial atom to relax. Relaxing the positions of the interstitial carbon will take about 20mins if we use a the unit cell (and much longer for a 2x2x2 cell):
```
&CONTROL
   calculation = 'relax' ! Variable cell relaxation
   nstep  = 50              ! Default number of optimisation steps
   forc_conv_thr = 1.0D-3   ! Ionic force convergence threshold (a.u.)
   etot_conv_thr = 1.0d-4   ! Ionic energy convergence threshold (a.u.)
   pseudo_dir = '.'
/

&SYSTEM
   ibrav = 0             ! Bravais Lattice index -- 2 for fcc
   nat = 8               ! Num of atoms in cell
   ntyp = 2              ! Num of atom types
   ecutwfc = 46.0        ! Cutoff energy in Rydberg, 46 suggested minimum
   occupations='smearing'! Smearing occupation for metals
   degauss = 1.0d-01     ! Smearing width in Ry
   smearing='mp'         ! Methfessel-Paxton smearing
/

&ELECTRONS
   conv_thr = 1.0d-8     ! SCF convergence in Ry
   mixing_beta = 0.7     ! SCF mixing factor
/

&IONS
  ion_dynamics = 'bfgs'  ! Broyden-Fletcher-Goldfarb-Shanno quasi-Newton method
/

ATOMIC_SPECIES
  Zr 91.224 Zr.pbe-spn-kjpaw_psl.1.0.0.UPF ! Species, mass, pseudopotential
  C  12.011 C.pbe-n-kjpaw_psl.1.0.0.UPF

CELL_PARAMETERS (ANGSTROM)
  4.706875154 0.000000 0.000000     ! A lattice vector
  0.000000 4.706875154 0.000000     ! B lattice vector
  0.000000 0.000000 4.706875154     ! C lattice vector

ATOMIC_POSITIONS crystal
  Zr 0.000000 0.000000 0.000000
  Zr 0.500000 0.500000 0.000000
  Zr 0.500000 0.000000 0.500000
  Zr 0.000000 0.500000 0.500000
  C  0.333333 0.333333 0.500000
  C  0.500000 0.000000 0.000000
  C  0.000000 0.500000 0.000000
  C  0.500000 0.500000 0.500000

K_POINTS automatic
  4 4 4 1 1 1             ! 2x2x2 k-point grid, offset by 1/2 in xyz
```
Estimate the concentration of this defect following the jupyter notebook [ZrC-point-defect-thermodynamics](.toMake/ZrC-point-defect-thermodynamics.pdf).

### 4. Aluminium vacancy formation energy including electron and phonon contributions
Estimate the formation energy of a vacancy in aluminium metal, accounting for the temperature dependent contributions of the electron and phonon free energies.

Steps:
1. Find the energy of the relaxed conventional unit cell of perfect Al.
2. Build a 2x2x2 Al supercell and compute phonons for perfect Al.
3. Create a vacancy in the 2x2x2 cell. Allow ionic positions to relax and find the total internal energy of the vacancy supercell.
4. Calculate phonons for the relaxed vacancy 2x2x2 Al suprecell using `phonopy`.
5. Calculate electronic density of states for perfect and vacancy 2x2x2 Al.
6. Compute the vacancy formation energy, including temperature dependent terms, using python.

#### Al unit cell total energy calculation
```
&CONTROL
   pseudo_dir = '.'
   calculation = 'scf'
/

&SYSTEM
   ibrav = 0      ! Bravais Lattice index -- 0 for free (give cell parameters)
   nat = 4        ! Num of atoms in cell
   ntyp = 1      ! Num of atom types
   ecutwfc = 29.0 ! Cutoff energy in Rydberg
   occupations='smearing'! Smearing occupation for metals
   degauss = 1.0d-01     ! Smearing width in Ry
   smearing='mp'         ! Methfessel-Paxton smearing
/

&ELECTRONS
   conv_thr = 1.0d-8  ! SCF convergence in Ry
   mixing_beta = 0.7  ! SCF mixing factor
   diagonalization = 'david'
/

K_POINTS automatic
4 4 4 1 1 1

#(base) tmellan@ImperialCollege:~/.../.toMake/10_al_phonon_dos$ echo 4.04292971*1.88973 | bc -l
#7.6400455608783
CELL_PARAMETERS (Bohr)
7.640045560878300     0.000000000000000    0.000000000000000
0.000000000000000     7.640045560878300    0.000000000000000
0.000000000000000     0.000000000000000    7.640045560878300

ATOMIC_SPECIES
 Al   26.98154   Al.pbe-nl-kjpaw_psl.1.0.0.UPF

ATOMIC_POSITIONS crystal ! fractional positions
Al 0.00 0.00 0.00
Al 0.50 0.50 0.00
Al 0.00 0.50 0.50
Al 0.50 0.00 0.50
```
#### 2x2x2 perfect Al supercell phonons
Use `phonopy` to make a 2x2x2 supercell, `phonopy --qe -d --dim="2 2 2" -c  1_al.in`, where `1_al.in` is the relaxed unit cell quantum-espresso input file. This will make a file called `supercell.in` that we can use to make our 2x2x2 supercell input file by containing with a 'header' file that has the extra information needed to run the calculation. 

The header file should look like:
```
&CONTROL
   pseudo_dir = '.'    ! Pseudopotentials directory (here)
   calculation = 'scf' ! Self-consistent field calculation
   tprnfor = .true.    ! calculate interatomoic forces
   tstress = .true.    ! calculate stresses

/

&SYSTEM
   ibrav = 0      ! Bravais Lattice index -- 0 for free (give cell parameters)
   nat = 32        ! Num of atoms in cell
   ntyp = 1      ! Num of atom types
   ecutwfc = 29.0 ! Cutoff energy in Rydberg
   occupations='smearing'! Smearing occupation for metals
   degauss = 1.0d-01     ! Smearing width in Ry
   smearing='mp'         ! Methfessel-Paxton smearing
/

&ELECTRONS
   conv_thr = 1.0d-9  ! SCF convergence in Ry
   mixing_beta = 0.7  ! SCF mixing factor
   diagonalization = 'david'
/

K_POINTS automatic
2 2 2 1 1 1
```

We can combine the header file with the supercell structure, run the quantum espresso calculation, compute the the force constant matrix, and phonon density of states with the following bash script:

```
#!/bin/bash

#Initial setup
# 1) Make 1_al.in unit cell input file
# -- The lattice parameter should be the relaxed one
# -- Set it in Bohr so phonopy knows how to deal with it
# 2) Make supercells with displacements: phonopy --qe -d --dim="2 2 2" -c  1_al.in
# 3) Make al_header.in file containing supercell calculation information

#Note
#Examplhttps://atztogo.github.io/phonopy/qe.htmle from the phonopy documentation can be found here:
#https://atztogo.github.io/phonopy/qe.html
#Within input files here:
#https://github.com/atztogo/phonopy/blob/master/example/NaCl-pwscf/NaCl-001.in

#Set the second index in the brace expansion to the number of supercells
for i in {001..001}; do
        cat al_header.in supercell-$i.in > 2_al_ph_$i.in
        pw.x < 2_al_ph_$i.in > 2_al_ph_$i.out &
        wait
done
wait
phonopy --qe -f 2_al_ph_001.out
wait
phonopy --qe -c 1_al.in -p band.conf
phonopy --qe -c 1_al.in -p mesh.conf
```
The phonopy density of states and and dispersion calculations require the parameter files `bands.conf`:
```
DIM = 2 2 2
BAND = 0.0 0.0 0.0   0.5 0.5 0.0  0.0 0.0 0.0  0.5 0.5 0.5
BAND_LABELS = $\Gamma$ X $\Gamma$ L $\Gamma$
BAND_POINTS = 101
```
ands `mesh.conf` for the phonon density of states:
```
ATOM_NAME = Al
DIM =  2 2 2
MESH = 25 25 25
```

#### 2x2x2 vacancy supercell DFT relaxation calculation

We can perform a vacancy supercell relaxation calculation with the parameters:
```
&CONTROL
   pseudo_dir = '.'    ! Pseudopotentials directory (here)
   calculation = 'relax' ! Self-consistent field calculation
   tprnfor = .true.    ! calculate interatomoic forces
   tstress = .true.    ! calculate stresses
   forc_conv_thr = 1.0D-4   ! Ionic force convergence threshold (a.u.)
   etot_conv_thr = 1.0d-5   ! Ionic energy convergence threshold (a.u.)
/

&SYSTEM
   ibrav = 0      ! Bravais Lattice index -- 0 for free (give cell parameters)
   nat = 31        ! Num of atoms in cell
   ntyp = 1      ! Num of atom types
   ecutwfc = 29.0 ! Cutoff energy in Rydberg
   occupations='smearing'! Smearing occupation for metals
   degauss = 1.0d-01     ! Smearing width in Ry
   smearing='mp'         ! Methfessel-Paxton smearing
/

&ELECTRONS
   conv_thr = 1.0d-9  ! SCF convergence in Ry
   mixing_beta = 0.7  ! SCF mixing factor
   diagonalization = 'david'
/

&IONS
  ion_dynamics = 'bfgs'  ! Broyden-Fletcher-Goldfarb-Shanno quasi-Newton method
/

K_POINTS automatic
2 2 2 1 1 1
!    ibrav = 0, nat = 32, ntyp = 1
CELL_PARAMETERS bohr
   15.2800911217566000    0.0000000000000000    0.0000000000000000
    0.0000000000000000   15.2800911217566000    0.0000000000000000
    0.0000000000000000    0.0000000000000000   15.2800911217566000
ATOMIC_SPECIES
 Al   26.98154   Al.pbe-nl-kjpaw_psl.1.0.0.UPF
ATOMIC_POSITIONS crystal
 Al   0.4999999999999999  0.0000000000000000  0.0000000000000000
 Al   0.0000000000000000  0.4999999999999999  0.0000000000000000
 Al   0.4999999999999999  0.4999999999999999  0.0000000000000000
 Al   0.0000000000000000  0.0000000000000000  0.4999999999999999
 Al   0.4999999999999999  0.0000000000000000  0.4999999999999999
 Al   0.0000000000000000  0.4999999999999999  0.4999999999999999
 Al   0.4999999999999999  0.4999999999999999  0.4999999999999999
 Al   0.2500000000000000  0.2500000000000000  0.0000000000000000
 Al   0.7500000000000000  0.2500000000000000  0.0000000000000000
 Al   0.2500000000000000  0.7500000000000000  0.0000000000000000
 Al   0.7500000000000000  0.7500000000000000  0.0000000000000000
 Al   0.2500000000000000  0.2500000000000000  0.4999999999999999
 Al   0.7500000000000000  0.2500000000000000  0.4999999999999999
 Al   0.2500000000000000  0.7500000000000000  0.4999999999999999
 Al   0.7500000000000000  0.7500000000000000  0.4999999999999999
 Al   0.0000000000000000  0.2500000000000000  0.2500000000000000
 Al   0.4999999999999999  0.2500000000000000  0.2500000000000000
 Al   0.0000000000000000  0.7500000000000000  0.2500000000000000
 Al   0.4999999999999999  0.7500000000000000  0.2500000000000000
 Al   0.0000000000000000  0.2500000000000000  0.7500000000000000
 Al   0.4999999999999999  0.2500000000000000  0.7500000000000000
 Al   0.0000000000000000  0.7500000000000000  0.7500000000000000
 Al   0.4999999999999999  0.7500000000000000  0.7500000000000000
 Al   0.2500000000000000  0.0000000000000000  0.2500000000000000
 Al   0.7500000000000000  0.0000000000000000  0.2500000000000000
 Al   0.2500000000000000  0.4999999999999999  0.2500000000000000
 Al   0.7500000000000000  0.4999999999999999  0.2500000000000000
 Al   0.2500000000000000  0.0000000000000000  0.7500000000000000
 Al   0.7500000000000000  0.0000000000000000  0.7500000000000000
 Al   0.2500000000000000  0.4999999999999999  0.7500000000000000
 Al   0.7500000000000000  0.4999999999999999  0.7500000000000000
```
This will take around 40 minutes. You can wait for it to finish or note that the total energy is -1224.52622079 Ry and the relaxed structure is:

```
ATOMIC_POSITIONS crystal
Al       0.500000000   0.000000000   0.000000000
Al       0.000000000   0.500000000   0.000000000
Al       0.500000000   0.500000000   0.000000000
Al       0.000000000   0.000000000   0.500000000
Al       0.500000000   0.000000000   0.500000000
Al       0.000000000   0.500000000   0.500000000
Al       0.500000000   0.500000000   0.500000000
Al       0.245416906   0.245416906   0.000000000
Al       0.754583094   0.245416906  -0.000000000
Al       0.245416906   0.754583094   0.000000000
Al       0.754583094   0.754583094   0.000000000
Al       0.250633947   0.250633947   0.500000000
Al       0.749366053   0.250633947   0.500000000
Al       0.250633947   0.749366053   0.500000000
Al       0.749366053   0.749366053   0.500000000
Al      -0.000000000   0.245416906   0.245416906
Al       0.500000000   0.250633947   0.250633947
Al      -0.000000000   0.754583094   0.245416906
Al       0.500000000   0.749366053   0.250633947
Al       0.000000000   0.245416906   0.754583094
Al       0.500000000   0.250633947   0.749366053
Al      -0.000000000   0.754583094   0.754583094
Al       0.500000000   0.749366053   0.749366053
Al       0.245416906   0.000000000   0.245416906
Al       0.754583094  -0.000000000   0.245416906
Al       0.250633947   0.500000000   0.250633947
Al       0.749366053   0.500000000   0.250633947
Al       0.245416906   0.000000000   0.754583094
Al       0.754583094   0.000000000   0.754583094
Al       0.250633947   0.500000000   0.749366053
Al       0.749366053   0.500000000   0.749366053
```

#### Vacancy phonon calculation for Al

Use the structure given above to setup an Al vacancy phonon calculation.  Run the phonon calculation with the following script:
```
#!/bin/bash

#Initial setup
# 1) Make 1_al.in unitcell input file. Note the vacancy unit cell in this instance is the 31 atom 2x2x2 perfect cell.
# -- The lattice parameter should be the relaxed one.
# -- Set it in Bohr so phonopy knows how to deal with it.
# 2) Make supercells with displacements: phonopy --qe -d --dim="1 1 1" -c  1_al.in. This will produce 7 displacements.
# 3) Make al_header.in file containing supercell calculation information

#Examplhttps://atztogo.github.io/phonopy/qe.htmle from the phonopy documentation can be found here:
#https://atztogo.github.io/phonopy/qe.html
#Within input files here:
#https://github.com/atztogo/phonopy/blob/master/example/NaCl-pwscf/NaCl-001.in

#Set the second index in the brace expansion to the number of supercells
batchJobs=4
c=0
for i in {001..007}; do
        cat al_header.in supercell-$i.in > 2_al_ph_$i.in
        pw.x < 2_al_ph_$i.in > 2_al_ph_$i.out &

#       wait
        #Wait for batches of $batchJobs to complete before starting next batch
        let c=c+1
        if (( $c % $batchJobs == 0 )); then wait; fi

done
wait
phonopy --qe -f 2_al_ph_{001..007}.out
wait
phonopy --qe -c 1_al.in -p band.conf
phonopy --qe -c 1_al.in -p mesh.conf
```

#### Electron DOS for perfect and vacancy supercell Al
Using the perfect and relaxed vacancy 2x2x2 supercells from the phonon calculations, set up input files for SCF, NSCF and DOS calculations. 

`1_al_scf.in` input file for SCF run:
```
&CONTROL
   pseudo_dir = '.'    ! Pseudopotentials directory (here)
   calculation = 'scf' ! Self-consistent field calculation
   tprnfor = .true.    ! calculate interatomoic forces
   tstress = .true.    ! calculate stresses
/

&SYSTEM
   ibrav = 0      ! Bravais Lattice index -- 0 for free (give cell parameters)
   nat = 31        ! Num of atoms in cell
   ntyp = 1      ! Num of atom types
   ecutwfc = 29.0 ! Cutoff energy in Rydberg
   occupations='smearing'! Smearing occupation for metals
   degauss = 1.0d-01     ! Smearing width in Ry
   smearing='mp'         ! Methfessel-Paxton smearing
/

&ELECTRONS
   conv_thr = 1.0d-8  ! SCF convergence in Ry
   mixing_beta = 0.7  ! SCF mixing factor
   diagonalization = 'david'
/

&IONS
  ion_dynamics = 'bfgs'  ! Broyden-Fletcher-Goldfarb-Shanno quasi-Newton method
/

K_POINTS automatic
2 2 2 1 1 1

CELL_PARAMETERS bohr
   15.2800911217566000    0.0000000000000000    0.0000000000000000
    0.0000000000000000   15.2800911217566000    0.0000000000000000
    0.0000000000000000    0.0000000000000000   15.2800911217566000
ATOMIC_SPECIES
 Al   26.98154   Al.pbe-nl-kjpaw_psl.1.0.0.UPF

ATOMIC_POSITIONS crystal
Al       0.500000000   0.000000000   0.000000000
Al       0.000000000   0.500000000   0.000000000
Al       0.500000000   0.500000000   0.000000000
Al       0.000000000   0.000000000   0.500000000
Al       0.500000000   0.000000000   0.500000000
Al       0.000000000   0.500000000   0.500000000
Al       0.500000000   0.500000000   0.500000000
Al       0.245416906   0.245416906   0.000000000
Al       0.754583094   0.245416906  -0.000000000
Al       0.245416906   0.754583094   0.000000000
Al       0.754583094   0.754583094   0.000000000
Al       0.250633947   0.250633947   0.500000000
Al       0.749366053   0.250633947   0.500000000
Al       0.250633947   0.749366053   0.500000000
Al       0.749366053   0.749366053   0.500000000
Al      -0.000000000   0.245416906   0.245416906
Al       0.500000000   0.250633947   0.250633947
Al      -0.000000000   0.754583094   0.245416906
Al       0.500000000   0.749366053   0.250633947
Al       0.000000000   0.245416906   0.754583094
Al       0.500000000   0.250633947   0.749366053
Al      -0.000000000   0.754583094   0.754583094
Al       0.500000000   0.749366053   0.749366053
Al       0.245416906   0.000000000   0.245416906
Al       0.754583094  -0.000000000   0.245416906
Al       0.250633947   0.500000000   0.250633947
Al       0.749366053   0.500000000   0.250633947
Al       0.245416906   0.000000000   0.754583094
Al       0.754583094   0.000000000   0.754583094
Al       0.250633947   0.500000000   0.749366053
Al       0.749366053   0.500000000   0.749366053
```

`2_al_nscf.in` input file for NSCF run:
```
&CONTROL
   pseudo_dir = '.'    ! Pseudopotentials directory (here)
   calculation = 'nscf' ! Self-consistent field calculation
   tprnfor = .true.    ! calculate interatomoic forces
   tstress = .true.    ! calculate stresses
/

&SYSTEM
   ibrav = 0      ! Bravais Lattice index -- 0 for free (give cell parameters)
   nat = 31        ! Num of atoms in cell
   ntyp = 1      ! Num of atom types
   ecutwfc = 29.0 ! Cutoff energy in Rydberg
   occupations='smearing'! Smearing occupation for metals
   degauss = 1.0d-01     ! Smearing width in Ry
   smearing='mp'         ! Methfessel-Paxton smearing
/

&ELECTRONS
   conv_thr = 1.0d-8  ! SCF convergence in Ry
   mixing_beta = 0.7  ! SCF mixing factor
   diagonalization = 'david'
/

&IONS
  ion_dynamics = 'bfgs'  ! Broyden-Fletcher-Goldfarb-Shanno quasi-Newton method
/

K_POINTS automatic
7 7 7 1 1 1

CELL_PARAMETERS bohr
   15.2800911217566000    0.0000000000000000    0.0000000000000000
    0.0000000000000000   15.2800911217566000    0.0000000000000000
    0.0000000000000000    0.0000000000000000   15.2800911217566000
ATOMIC_SPECIES
 Al   26.98154   Al.pbe-nl-kjpaw_psl.1.0.0.UPF

ATOMIC_POSITIONS crystal
Al       0.500000000   0.000000000   0.000000000
Al       0.000000000   0.500000000   0.000000000
Al       0.500000000   0.500000000   0.000000000
Al       0.000000000   0.000000000   0.500000000
Al       0.500000000   0.000000000   0.500000000
Al       0.000000000   0.500000000   0.500000000
Al       0.500000000   0.500000000   0.500000000
Al       0.245416906   0.245416906   0.000000000
Al       0.754583094   0.245416906  -0.000000000
Al       0.245416906   0.754583094   0.000000000
Al       0.754583094   0.754583094   0.000000000
Al       0.250633947   0.250633947   0.500000000
Al       0.749366053   0.250633947   0.500000000
Al       0.250633947   0.749366053   0.500000000
Al       0.749366053   0.749366053   0.500000000
Al      -0.000000000   0.245416906   0.245416906
Al       0.500000000   0.250633947   0.250633947
Al      -0.000000000   0.754583094   0.245416906
Al       0.500000000   0.749366053   0.250633947
Al       0.000000000   0.245416906   0.754583094
Al       0.500000000   0.250633947   0.749366053
Al      -0.000000000   0.754583094   0.754583094
Al       0.500000000   0.749366053   0.749366053
Al       0.245416906   0.000000000   0.245416906
Al       0.754583094  -0.000000000   0.245416906
Al       0.250633947   0.500000000   0.250633947
Al       0.749366053   0.500000000   0.250633947
Al       0.245416906   0.000000000   0.754583094
Al       0.754583094   0.000000000   0.754583094
Al       0.250633947   0.500000000   0.749366053
Al       0.749366053   0.500000000   0.749366053
```

`3_al_dos.in` input to calculate the density of states. 
```
&DOS
  degauss = 0.03
  DeltaE = 0.005
 /
```
#### Compute the vacancy formation energy, including temperature dependent terms
Open a python notebook (`jupyter notebook`), and follow the python code outlined [here](.toMake/07_al_vacancy/al_vac_free_energy.pdf).

The notebook relies on electron and phonon density of states for Al. You can use your own calculated ones, or the following ones: [Al perfect phonon DOS](.toMake/07_al_vacancy/02_al_222/total_dos.dat), [Al vacancy phonon DOS](.toMake/07_al_vacancy/04_al_222_vac_relaxed_scf/total_dos.dat), [Al perfect electron DOS](.toMake/07_al_vacancy/06_al_222_perfect_el_dos/dos_shifted.dat), and [Al vacancy electron DOS](.toMake/07_al_vacancy/05_al_222_vac_relaxed_scf_el_dos/dos_shifted.dat).

-----------
Summary
-----------
In the lab we have covered how to create a supercell from a relaxed unit cell, perform point defect DFT calculations with vacancies, Frenkel and anti-site defects, and compute the defect concentration vs T. 
In addition for Aluminium, we have calculated the vacancy formation energy, including temperature-dependent contributions from the electron and phonon free energies.
