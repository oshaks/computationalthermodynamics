#!/bin/bash

rydberg_to_eV="13.6056980659"
area=$(echo 4.04292971*4.04292971 | bc -l)
e_perfect=$(grep ! 01_al_unitcell/*out | awk '{print $5}')
e_vacslab_scf=$(grep ! 02_al_surface_manual_scf/*out | awk '{print $5}')
e_vacslab_relax=$(grep ! 03_al_surface_manual_relax/*out | tail -n 1 | awk '{print $5}')

e_surf_scf=$(echo $rydberg_to_eV*0.5*$e_vacslab_scf/$area - $rydberg_to_eV*$e_perfect/$area | bc -l)
e_surf_relax=$(echo $rydberg_to_eV*0.5*$e_vacslab_relax/$area - $rydberg_to_eV*$e_perfect/$area | bc -l)

echo "Surface energy, " $e_surf_scf, "eV/Ang^2"
echo "Relaxed surface energy, " $e_surf_relax, "eV/Ang^2"

