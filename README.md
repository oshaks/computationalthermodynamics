Computational Thermodynamics for Defects in Crystals
==================================================

This is the lab material for the [East African Institute for Fundamental Research](https://eaifr.ictp.it/) course, "Defects in Crystals", November 2019.

The lab sessions cover some of the computational techniques used to study Defects in Crystals. Our field of vision is restricted to an introduction to the *ab initio* approach to study the thermodynamics of defects in  crystals, in order to keep the course self-contained.
The computational thermodynamics defect systems will be determined from statistical mechanical principles using total energies, interatomic forces and eigenvalues, computed quantum mechanically using density functional theory (DFT). 
DFT calculations for moderate sized systems are computationally expensive, within the the context of hours of computation time on a workstation. Despite the approximations required to get results quickly, the calculations we consider will still give meaningful insight, and hopefully act a useful starting point for larger-scale simulations.

The practical calculations exclusively rely on open-source software, such as the bash shell and [Python3](https://www.python.org/) with [Jupyter notebooks](https://jupyter.org/) for scripting, and the DFT code [Quantum Espresso](http://www.quantum-espresso.org/) to do the computational heavy lifting. Open source codes are used so they can continue to be freely used after the course, hopefully making the practical sessions a useful point of entry for anyone who wishes to continue research in the field. My aim in putting together the practical material was that it be useful to a Master's or PhD level student starting research in this area, based on my own experience of what I would have found useful. 

The labs are organised as follows:
- [Lab 1: The Linux Shell, scripting, and plotting](lab01)
- [Lab 2: DFT calculations with Quantum Espresso](lab02)
- [Lab 3: Point defect DFT calculations](lab03)
- [Lab 4: Interface DFT calculations](lab04)
- [Lab 5: Final session](lab05)

Software
--------
- DFT: [quantum-espresso](https://www.quantum-espresso.org/), in particular the `pw.x`, `dos.x` and `bands.x` executables. The quantum-espresso docs are [online here](https://www.quantum-espresso.org/Doc/INPUT_PW.htm).
- Numerical and scientific computing: [numpy](https://numpy.org/) and [scipy](https://www.scipy.org/), via [python3](https://www.python.org/) using [jupyter interactive notebooks](https://jupyter.org/).
- Atomic simulation tools: [Atomic Simulation Environment (ASE)](https://wiki.fysik.dtu.dk/ase/).
- Shell: [bash](https://www.gnu.org/software/bash/manual/).
- Visualisation: [xcrysden](http://www.xcrysden.org/), [VESTA](https://jp-minerals.org/vesta/en/).
- Plotting: [gnuplot](http://www.gnuplot.info/), [python matplotlib](https://matplotlib.org/).
- Phonons: [phonopy](https://atztogo.github.io/phonopy/).

Useful resources
--------------------
- The quantum-espresso [docs](https://www.quantum-espresso.org/Doc/INPUT_PW.htm).
- [FU-Berlin](http://kirste.userpage.fu-berlin.de/chemistry/general/units_en.html) has a useful page for unit conversion.
- For more of an introduction to Linux, try the excellent Software Carpentry [Linux Shell](https://swcarpentry.github.io/shell-novice/) course lectures.
- Our lab sessions draw inspiration from the [Materials Modelling](https://gitlab.com/eamonnmurray/MaterialsModelling) course at Imperial College.
- The MIT course on [Atomistic Modelling](https://ocw.mit.edu/courses/materials-science-and-engineering/3-320-atomistic-computer-modeling-of-materials-sma-5107-spring-2005/) has useful lecture notes and also uses Quantum Espresso. 


#### _To do_
1. Rerun lab3 Al NSCF vacancy electron DOS calculation, with NSCF, and with finer sampling
2. Rerun lab3 Al phonon perfect DOS cal, in "07_al_vacancy/02_al_222"
